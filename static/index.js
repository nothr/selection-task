let btn = document.getElementById("btn");
let inc = 0;
let clrs = ["#A18594", "#ED760E", "#B32428"];

btn.addEventListener("click", () => {
  inc++;
  let randomColor = Math.floor(Math.random() * 3);
  btn.innerHTML = inc;
  btn.style.backgroundColor = clrs[randomColor];
});
